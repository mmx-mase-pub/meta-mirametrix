DESCRIPTION = "Mase"
AUTHOR = "Mirametrix"
HOMEPAGE = "https://mirametrix.com/"
LICENSE = "CLOSED"

COMPATIBLE_MACHINE = "(aarch64|x86_64)"

SRC_URI = "\
    file://mirametrix-mase.${TARGET_ARCH}.rpm;subdir=${BP} \
    file://mirametrix-mase-dev.${TARGET_ARCH}.rpm;subdir=${BP} \
    file://mirametrix-mase-tools.${TARGET_ARCH}.rpm;subdir=${BP} \
"

# You must copy the RPM archives provided by Mirametrix in a "mase-packages"
# directory inside the "build" directory or provide another path to those RPM
# archives by prepending to FILESEXTRAPATHS.
#
FILESEXTRAPATHS_append = "${TOPDIR}/mase-packages/"

do_install() {
    cp -rf ${S}/* ${D}
}

PACKAGES += " ${PN}-tools"

FILES_${PN} = "\
    ${datadir}/mmx-mase/Classifiers/* \
    ${datadir}/mmx-mase/model/* \
    ${datadir}/mmx-mase/CameraLibrary/* \
    ${datadir}/mmx-mase/ProcTreeXmls/* \
    ${datadir}/mmx-mase/MaseLogConfig.date \
    ${datadir}/mmx-mase/SystemConfig.xme \
    ${libdir}/*.so.* \
    ${libdir}/mmx-mase/*.mtp* \
"

FILES_${PN}-dev = "\
    ${includedir}/MaseSDK/* \
    ${libdir}/cmake/MaseSDK* \
    ${datadir}/examples/cpp/* \
    ${libdir}/lib*.so \
"

FILES_${PN}-tools = "\
    ${bindir}/linux_start_main.sh \
    ${bindir}/MaseService* \
    ${datadir}/examples/Python/* \
"

RDEPENDS_${PN} = "\
    tensorflow-lite \
    log4cplus \
    util-linux-libuuid \
    libcurl \
    libopencv-dnn \
    libopencv-video \
    libopencv-objdetect \
    libopencv-calib3d \
    libopencv-highgui \
    libopencv-videoio \
    libopencv-imgcodecs \
    libopencv-photo \
    libopencv-imgproc \
    libopencv-core \
    libcrypto \
    libssl \
    vimba \
 "

# Insight tool is python based and needs python3 at runtime
RDEPENDS_${PN}-tools = "\
    log4cplus \
    python3 \
    python3-lxml \
    python3-cycler \
    python3-kiwisolver \
    python3-psutil \
    python3-pyparsing \
    python3-pillow \
    python3-dateutil \
    python3-screeninfo \
"

INSANE_SKIP_${PN} += "already-stripped"
