DESCRIPTION = "Mase yocto demo"
AUTHOR = "Mirametrix"
HOMEPAGE = "https://mirametrix.com/"
LICENSE = "CLOSED"

# This recipe is to demonstrate how to create a software that build/link and rely on MASE 
# to run

COMPATIBLE_MACHINE = "(aarch64|x86_64)"

SRC_URI = "\
    git://bitbucket.org/mmx-mase-pub/mase-yocto-demo;branch=master;protocol=https \
"

S = "${WORKDIR}/git"
SRCREV = "${AUTOREV}"

DEPENDS = "\
    tensorflow-lite \
    opencv \
    log4cplus \
    mase-prebuilt \
"

RDEPENDS_${PN} = "\
    mase-prebuilt \
"

inherit cmake

OECMAKE_GENERATOR = "Unix Makefiles"

